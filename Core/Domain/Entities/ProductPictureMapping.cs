﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain.Entities
{
    /// <summary>
    /// Represents a product picture mapping
    /// </summary>
    public class ProductPictureMapping
    {
        /// <summary>
        /// Gets or sets the entity identifier
        /// </summary>
        public int PkProductPictureMappingId { get; set; }

        /// <summary>
        /// Gets or sets the product identifier
        /// </summary>
        public int FkProductId { get; set; }

        /// <summary>
        /// Gets or sets the picture identifier
        /// </summary>
        public int FkPictureId { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets the picture
        /// </summary>
        public virtual Picture Picture { get; set; }

        /// <summary>
        /// Gets the product
        /// </summary>
        public virtual Product Product { get; set; }
    }
}
