﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Domain.Config
{
    public class ProductConfig : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable(nameof(Product));
            builder.HasKey(product => product.PkProductId);

            builder.Property(product => product.Name).HasMaxLength(400).IsRequired();
            builder.Property(product => product.Price).HasColumnType("decimal(18, 4)");
            builder.Property(product => product.Weight).HasColumnType("decimal(18, 4)");
            builder.Property(product => product.Length).HasColumnType("decimal(18, 4)");
            builder.Property(product => product.Width).HasColumnType("decimal(18, 4)");
            builder.Property(product => product.Height).HasColumnType("decimal(18, 4)");
        }
    }

    public class ProductPictureMappingConfig : IEntityTypeConfiguration<ProductPictureMapping>
    {
        public void Configure(EntityTypeBuilder<ProductPictureMapping> builder)
        {
            builder.ToTable(nameof(ProductPictureMapping));
            builder.HasKey(productPicture => productPicture.PkProductPictureMappingId);

            builder.HasOne(productPicture => productPicture.Picture)
                .WithMany()
                .HasForeignKey(productPicture => productPicture.FkPictureId)
                .IsRequired();

            builder.HasOne(productPicture => productPicture.Product)
                .WithMany(product => product.ProductPictures)
                .HasForeignKey(productPicture => productPicture.FkProductId)
                .IsRequired();
        }
    }
}
