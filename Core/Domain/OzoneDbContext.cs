﻿using Core.Domain.Entities;
using Core.Domain.Entities.User;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain
{
    public class OzoneDbContext : DbContext
    {
        public OzoneDbContext(DbContextOptions<OzoneDbContext> options)
            : base(options)
        {
        }

        // Users
        public DbSet<User> Users { get; set; }

        // Products
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<PictureBinary> PictureBinary { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductPictureMapping> ProductPictureMappings { get; set; }


        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries<AuditInfo>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = string.Empty;
                        entry.Entity.Created = DateTime.Now;
                        break;
                    case EntityState.Modified:
                        entry.Entity.LastModifiedBy = string.Empty;
                        entry.Entity.LastModified = DateTime.Now;
                        break;
                }
            }

            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(OzoneDbContext).Assembly);
        }
    }
}
