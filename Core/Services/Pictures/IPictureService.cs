﻿using Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Services.Pictures
{
    /// <summary>
    /// Picture service interface
    /// </summary>
    public partial interface IPictureService
    {
        /// <summary>
        /// Get product picture (for shopping cart and order details pages)
        /// </summary>
        /// <param name="product">Product</param>
        /// <param name="attributesXml">Attributes (in XML format)</param>
        /// <returns>Picture</returns>
        Picture GetProductPicture(Product product, string attributesXml);
    }
}
