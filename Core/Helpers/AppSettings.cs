﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Helpers
{
    /// <summary>
    /// This is a public method that encapsulate the properties defined in the appsettings.json file
    /// </summary>
    /// <remarks>
    /// Used for accessing application settings via objects that are injected into classes using the ASP.NET Core built in dependency injection (DI) system.
    /// </remarks>
    public class AppSettings
    {
        public string Secret { get; set; }
        public string ConnectionString { get; set; }
    }
}
