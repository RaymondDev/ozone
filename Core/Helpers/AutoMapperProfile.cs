﻿using AutoMapper;
using Core.Domain.Entities.User;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserModel>();
            CreateMap<RegisterModel, User>();
            CreateMap<UpdateModel, User>();
        }
    }
}
