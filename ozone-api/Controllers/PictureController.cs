﻿using Core.Services.Pictures;
using Core.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Domain;

namespace ozone_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PictureController : BaseController
    {
        private readonly IPictureService _picture;

        public PictureController(
        OzoneDbContext context,
        IPictureService picture
        ) : base(context)
        {
            _picture = picture;
        }

        [HttpGet("apitest")]
        public IActionResult SecurityTest()
        {
            return Ok("Welcome");
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_picture.GetProductPicture(new Product(), ""));
        }
    }
}
