﻿using Core.Domain;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ozone_api.Controllers
{
    public class BaseController : Controller
    {
        protected readonly OzoneDbContext dbContext;

        public BaseController(OzoneDbContext context)
        {
            this.dbContext = context;
        }
    }
}
