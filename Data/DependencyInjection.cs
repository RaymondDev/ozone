﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Data
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddDataPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<OzoneDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("OzoneDbConnection")));

            return services;
        }
    }
}
